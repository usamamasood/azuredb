# Databricks notebook source
# Standard code from the tutorial to enable parameter passing
dbutils.widgets.text("JsonOutput", "","")
dbutils.widgets.get("JsonOutput")
y = getArgument("JsonOutput")
#print ("Param -\'JsonOutput:")

print(y)

# COMMAND ----------

#remove the backspace from the string so it passes as json
z = y.replace('\\', '')

# COMMAND ----------

#create some standard variables for the top level equipment - used in datalake calls
print("Equipment Details")
import json
data = json.loads(z)
PhysicalIdentifier = data['PhysicalIdentifier']
Site = data["SiteFolderName"]
print("EquipmentId = ", PhysicalIdentifier)
print("Site name = ", Site)

# COMMAND ----------

# #Connection strings for Data Lake
# spark.conf.set("dfs.adls.oauth2.access.token.provider.type", "ClientCredential")
# spark.conf.set("dfs.adls.oauth2.client.id", "f6a6b742-0829-4f4a-b485-28ded790a117")
# spark.conf.set("dfs.adls.oauth2.credential", "1jRv9NKpjrAlV2b5GJTaYh0/hP1kMRElQAaeShtxomw=")
# spark.conf.set("dfs.adls.oauth2.refresh.url", "https://login.microsoftonline.com/b771cb47-279a-4b84-aaeb-14a9b7a71446/oauth2/token")

# COMMAND ----------

#Helper Functions
from pyspark.sql.functions import to_timestamp
from pyspark.sql.functions import unix_timestamp
from pyspark.sql.functions import date_format
from pyspark.sql.types import TimestampType
import datetime
import time
from pyspark.sql.functions import udf,col

stripTZ = udf(lambda col : col[-7:])
stripDateTime = udf(lambda col : col[:-7])

def getHours(site, equipment_id, startDate, endDate):
  """Query the data lake for a given piece of equipment and return the operating hours
  
  Args:
  site -- the site for the data lake path
  equipment_id -- the phsycal identifier for the dat lake path
  startDate -- the date from which to start the query
  endDate -- the date from which to end the query
  
  Returns a tuple of HoursOperating and HoursOperatingWhileMonitored
  
  """
  startDate = datetime.datetime.strptime(startDate, '%Y-%m-%d %H:%M:%S')
  endDate = datetime.datetime.strptime(endDate, '%Y-%m-%d %H:%M:%S')
  path_adls = "adl://us165pqadls01.azuredatalakestore.net/{site}/{equipment_id}/Metrics/Raw/Csv".format(site=site, equipment_id=equipment_id)
  df_in =spark.read.format('csv').options(header='true', inferSchema='true').load(path_adls)
  df_cols = df_in.select('RecordStart', 'Metrics_EquipmentOperating')
  df_datetime = df_cols.withColumn("TimeZone", stripTZ('RecordStart'))
  df_datetime2 = df_datetime.withColumn("RecordStartDateTime", stripDateTime('RecordStart'))
  #Convert datetime sting to spark datestamp
  df_DateStamp = (df_datetime2.withColumn("RecordStartDateTime", df_datetime2['RecordStartDateTime'].cast("timestamp")))
  df_filtered = df_DateStamp.where((df_DateStamp.RecordStartDateTime > startDate) & (df_DateStamp.RecordStartDateTime < endDate))
  HoursMonitored = df_filtered.count()
  HoursOperatingWhileMonitored = df_filtered.filter(df_filtered['Metrics_EquipmentOperating']==1).count()
  ans = {"HoursMonitored" : HoursMonitored, "HoursOperatingWhileMonitored": HoursOperatingWhileMonitored}
  return ans
  #return schema

# COMMAND ----------

hours = getHours(Site, PhysicalIdentifier, '2018-03-27 00:00:00', '2018-03-27 23:59:59' )
print(hours)

# COMMAND ----------

def calculateRUL(PhysicalIdentifier, component, hours,estimatedTotalLife):
  """Calculate remaining usefull life statistics
  
  Args:
  PhysicalIdentifier -- the equipment id
  component -- eg impeller or throatbush
  hours -- a touple of HoursOperating and HoursOperatingWhileMonitored
  estimatedTotalLife -- expected life from history
  
  Returns the following json string of parameters
  
  componentRul = {
                        'MessageId': str(messageId),
                        'EquipmentId': row['EquipmentPhysicalIdentifier'],
                        'PhysicalIdentifier': row['EquipmentPhysicalIdentifier'],
                        'EquipmentUniqueName': row['EquipmentUniqueName'],
                        'EquipmentDescription': row['EquipmentDescription'],
                        'ComponentName': row['ComponentName'],
                        'GeneratedAt': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f'),
                        'InstalledAt': datetime_installedAt.strftime('%Y-%m-%d %H:%M:%S.%f'),
                        'InstalledAsString': installedAt,
                        'EstimatedRemainingUsefulLife': estimatedRemainingLife,
                        'EstimatedRemainingPercentUsefulLife': estimatedPercentRul,
                        'EstimatedTotalLife': row['EstimatedTotalLife'],
                        'ReplaceAt': replaceAt.strftime('%Y-%m-%d %H:%M:%S.%f'),
                        'ActualAvailability': actualAvailability,
                        'ActualOperatingHours': estimated_operating_hours,
                        'PercentUtilization': percentUtilisation,
                        'RemainingUsefulLifeLevel': 'Normal',
                        'RemainingUsefulLifeTrend': 'Steady',
                        'IsComponent' : isComponent} 
  """
  PhysicalIdentifier = PhysicalIdentifier
  ComponentName = "Component"
  HoursMonitored = hours['HoursMonitored']
  HoursOperatingWhileMonitored = hours['HoursOperatingWhileMonitored']
  return(HoursMonitored)
  
  

# COMMAND ----------

hours = getHours(Site, PhysicalIdentifier, '2018-03-23 00:00:17', '2018-03-27 00:05:00' )
rul = calculateRUL(PhysicalIdentifier, 'component', hours)
print(rul)

# COMMAND ----------

# import datetime as dt
# components = data['Component']
# for component in components:
#   startDate = (component['MaintenanceDate'])
#   now = dt.datetime.isoformat(dt.datetime.now())
#   #hoursMonitored = 
#   #hoursOperatingWhileMonitored = 
#   print(startDate)
#   print(now)


# COMMAND ----------

