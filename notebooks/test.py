import pytest
import logging
import json
import uuid
from weir_synertrex_seca_analytics.log_analytics import LogAnalyticsHander
from weir_synertrex_seca_analytics.config import Config
import os

def test_log_analytics_log():
    """
    This test is an example of how to setup logger to log to LogAnalytics
    It always passes when a log message is sent to OMS instance as I couldn't work out how to test accepted
    """

    # ----------  Logger Config Variables ----------  

    # Update the customer ID to your Log Analytics workspace ID
    customer_id = 'bc3bec22-4746-4c86-9899-2afafce22c1a'
    # For the shared key, use either the primary or the secondary Connected Sources client authentication key   
    shared_key = 'z4sj9xFQr72IDhzxdIowVYHW9FemfQIE8JDfigCrxT/yjOV9bp3sksYClQuLOjNAQA7ovGyL1FqJlNuPYcL2qg=='
    # The log type is the name of the event that is being submitted
    log_type = 'PythonLogAnalyticsTest'
    log_level_string = 'debug'
    log_level = logging.getLevelName(log_level_string.upper())
    
    # ----------  Setup Log Handlers  ----------  
    console_handler = logging.StreamHandler()    
    log_analytics_handler = LogAnalyticsHander(customer_id = customer_id,
                                            shared_key = shared_key,
                                            log_type = log_type)    
       
    # ----------- Setup Logger ---------------
    logger = logging.getLogger(__name__)
    logger.addHandler(console_handler)
    logger.addHandler(log_analytics_handler)
    logger.setLevel(log_level)

    # ---------- Setup Additional Properties for logging message    
    additional_properties = {        
        "logModule": "customAnalytics",                
        "logProperties_fieldGatewayId" : "dummy"        
    }

    log_analytics_handler.additional_properties = additional_properties
    
    # ---------- Test Logger     
    logger.info("Test info log event") #Should send info level message as log_level is at DEBUG
    logger.debug("Test debug log event") #Should send debug level message as log_level is at DEBUG
    logger.setLevel(logging.INFO) #update level of logger
    logger.debug("Test debug log event changed log level to info")  #Should not send message

    assert 0==0

def test_log_analytics_from_config():

    config_filepath = r'tests\data\customAnalyticsConfig.json'
    if os.path.isfile(config_filepath ): 
        os.remove(config_filepath)

    config = Config(config_filepath)
    config_parameters = config.config_dict
    print(config_parameters)
    # ----------  Setup Log Handlers  ----------  
    console_handler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')    
    console_handler.setFormatter(formatter)
    log_analytics_handler = LogAnalyticsHander(customer_id =config_parameters['logging_parameters']['customer_id'],
                                            shared_key = config_parameters['logging_parameters']['shared_key'], 
                                            log_type = config_parameters['logging_parameters']['log_type'])

    # ---------- Setup Additional Properties for logging message        
    log_analytics_handler.additional_properties = config_parameters['logging_parameters']['additional_properties']

    # ----------- Setup Logger ---------------
    logger = logging.getLogger("mainLogger")    
    logger.addHandler(console_handler)
    # logger.addHandler(log_analytics_handler)
    logger.setLevel(config_parameters['logging_parameters']['log_level'])
    
    config.logger = logger
    logger.info("finish init")

def test_log_analytics_correlation_id():
    """
    This test is an example of how to setup logger to log to LogAnalytics
    It always passes when a log message is sent to OMS instance as I couldn't work out how to test accepted
    """

    # ----------  Logger Config Variables ----------  

    # Update the customer ID to your Log Analytics workspace ID
    customer_id = 'bc3bec22-4746-4c86-9899-2afafce22c1a'
    # For the shared key, use either the primary or the secondary Connected Sources client authentication key   
    shared_key = 'z4sj9xFQr72IDhzxdIowVYHW9FemfQIE8JDfigCrxT/yjOV9bp3sksYClQuLOjNAQA7ovGyL1FqJlNuPYcL2qg=='
    # The log type is the name of the event that is being submitted
    log_type = 'PythonLogAnalyticsTest'
    log_level_string = 'debug'
    log_level = logging.getLevelName(log_level_string.upper())
    
    # ----------  Setup Log Handlers  ----------  
    console_handler = logging.StreamHandler()    
    log_analytics_handler = LogAnalyticsHander(customer_id = customer_id,
                                            shared_key = shared_key,
                                            log_type = log_type)    
       
    # ----------- Setup Logger ---------------
    logger = logging.getLogger(__name__)
    logger.addHandler(console_handler)
    logger.addHandler(log_analytics_handler)
    logger.setLevel(log_level)

    # ---------- Setup Additional Properties for logging message
    correlation_id = str(uuid.uuid4())        
    additional_properties = {
        "logModule": "customAnalytics",
        "logProperties_fieldGatewayId" : "dummy"
    }

    log_analytics_handler.additional_properties = additional_properties
    
    # ---------- Test Logger     
    logger.info("Test info log event",extra = {'correlation_id':correlation_id}) #Should send info level message as log_level is at DEBUG
    
    assert 0==0

