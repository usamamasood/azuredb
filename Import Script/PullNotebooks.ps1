﻿ param
    (        
        [Parameter(Mandatory=$true)]
        [string] $AccessToken,
        [Parameter(Mandatory=$true)]
        [string] $DestinationPath,
        [Parameter(Mandatory=$true)]
        [string] $SourcePath
    )  

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$global:listOfFiles = @()

#This method traverse through directory and generates the list of notebooks found in the lake at any level.
function Get-AllRepositoryFiles
{
    param
    (        
        [Parameter(Mandatory=$true)]
        [string] $Path	
    )    
	
    $itemList = Get-ChildItem  -Path $Path
    
    foreach($item in $itemList)
    {   $Name=$item.FullName;
				
		if ($item.Attributes -ieq "ARCHIVE")
		{ 
            $global:listOfFiles += $Name
		    Write-Output "$Name"		
		}
		if ($item.Attributes -ieq "DIRECTORY")
		{
		    Get-AllRepositoryFiles -Path $Name
		}				
    }		
}


function Upload-Notebook {

    param (
    [string]$accessToken,
    [string]$filePath,
    [string]$DestinationPath)

    #---- Get the Content of file into this variable
    $content = Get-Content -Path $filePath -Raw
    
    #---- Encoding the string to Base 64 String
    $encodingUTF8  = [System.Text.Encoding]::UTF8.GetBytes($content)
    $encodedBase64String = [System.Convert]::ToBase64String($encodingUTF8)
                  
    #---- Setting up The parameters of Post function    

    $programmingLanguage = Get-Language -filePath $filePath

    $uri = "https://centralus.azuredatabricks.net/api/2.0/workspace/import"
    $importPath = $DestinationPath + $filePath.Remove(0, $filePath.LastIndexOf('\')).Replace('\','/')

    $headers=@{}
    $headers.Add("Authorization", "Bearer $accessToken")
    $Body = @{    
        content = $encodedBase64String
        path = $importPath 
        language = $programmingLanguage
        overwrite = $true
        format = "SOURCE"    
    }

    if ($programmingLanguage -ne $null -and $ImportPath -ne $null -and $accessToken)
    {
        Write-Host "Uploading  $Body"
        Invoke-RestMethod -Method Post -Uri $uri -Headers $headers -Body ($Body | ConvertTo-Json)
    }
}

function Get-Language {
    param (    
    [string]$filePath)
    
    if ($filePath.EndsWith('.scala') -or $filePath.EndsWith('.sc'))
    {
        return "SCALA"
    }    
    elseif ($filePath.EndsWith('.sql'))
    {
        return "SQL"
    }    
    elseif ($filePath.EndsWith('.r') -or $filePath.EndsWith('.R') -or $filePath.EndsWith('.rData') )
    {
        return "R"
    }    
    elseif ($filePath.EndsWith('.py'))
    {
        return "PYTHON"
    }    
}

Write-Host "Script Started"

Get-AllRepositoryFiles -Path $SourcePath

Write-Host "Files Paths Gathered"

if ($global:listOfFiles.count -gt 0 )
{
     foreach($item in [Array]$global:listOfFiles)
    {
        Upload-Notebook -accessToken $AccessToken -filePath $item -DestinationPath $DestinationPath
    }		
}